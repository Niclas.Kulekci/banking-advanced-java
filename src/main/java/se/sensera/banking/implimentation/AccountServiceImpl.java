package se.sensera.banking.implimentation;

import lombok.AllArgsConstructor;
import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

@AllArgsConstructor
public class AccountServiceImpl implements AccountService
{
    AccountsRepository accountsRepository;
    UsersRepository usersRepository;

    @Override
    public Account createAccount(String userId, String accountName) throws UseException
    {
        Optional<User> user = usersRepository.getEntityById(userId);
        if (user.isPresent())
        {
            AccountImpl account = new AccountImpl(UUID.randomUUID().toString(), user.get(), accountName, true, new LinkedList<>());
            checkUserAccountAlreadyExists(account);
            return accountsRepository.save(account);
        } else
            {
            throw new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND);
        }
    }

    private void checkUserAccountAlreadyExists(AccountImpl account) throws UseException
    {
        if (accountsRepository.all().anyMatch(account1 -> account.getName().equalsIgnoreCase(account.name)))
        {
            throw new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
        }
    }

    @Override
    public Account changeAccount(String userId, String accountId, Consumer<ChangeAccount> changeAccountConsumer) throws UseException
    {
//        Account account = accountsRepository.getEntityById(accountId).get();
//
//        if(account.getId().equalsIgnoreCase(accountId))
//        {
//         throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
//        }
//        changeAccountConsumer.accept(account::setName);
//        return accountsRepository.save(account);

        User accountOwner = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NOT_FOUND));
        if (!account.getOwner().equals(accountOwner))
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (!account.isActive())
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);
        }
        boolean approvedChange = new changeAccount().approved(changeAccountConsumer, account);
        return approvedChange ? accountsRepository.save(account) : account;
    }

    class changeAccount
    {
        boolean approved = false;
        public boolean approved(Consumer<ChangeAccount> changeAccountConsumer, Account account)
        {
            changeAccountConsumer.accept(name ->
            {
                if (accountsRepository.all().anyMatch(account1 -> account1.getName().equals(name)))
                    throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NAME_NOT_UNIQUE);
                if (!account.getName().equals(name))
                {
                    account.setName(name);
                    approved = true;
                }
            });
            return approved;
        }
    }

    @Override
    public Account addUserToAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException
    {
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_FOUND));
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        User addUser = usersRepository.getEntityById(userIdToBeAssigned)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        if (!account.isActive())
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.ACCOUNT_NOT_ACTIVE); //?WHY!?!?!
        }
        if (account.getOwner().equals(addUser))
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.CANNOT_ADD_OWNER_AS_USER);
        }
        if (!account.getOwner().equals(user))
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (account.getUsers().anyMatch(userRes -> userRes.equals(addUser)))
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.USER_ALREADY_ASSIGNED_TO_THIS_ACCOUNT);

        account.addUser(addUser);
        accountsRepository.save(account);
        return account;
    }

    @Override
    public Account removeUserFromAccount(String userId, String accountId, String userIdToBeAssigned) throws UseException
    {
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_FOUND));
        User newUser = usersRepository.getEntityById(userIdToBeAssigned)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        if (!account.getOwner().equals(user))
        {
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (account.getUsers().noneMatch(userRes -> userRes.getId().equals(userIdToBeAssigned)))
            throw new UseException(Activity.UPDATE_ACCOUNT, UseExceptionType.USER_NOT_ASSIGNED_TO_THIS_ACCOUNT);

        accountsRepository.save(account);
        account.removeUser(newUser);
        return account;
    }

    @Override
    public Account inactivateAccount(String userId, String accountId) throws UseException
    {
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_FOUND));
        User owner = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.USER_NOT_FOUND));
        if (!account.getOwner().equals(owner))
        {
            throw new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_OWNER);
        }
        if (!account.isActive() || !owner.isActive())
        {
            throw new UseException(Activity.INACTIVATE_ACCOUNT, UseExceptionType.NOT_ACTIVE);
        }
        account.setActive(false);
        return accountsRepository.save(account);
    }

    @Override
    public Stream<Account> findAccounts(String searchValue, String userId, Integer pageNumber, Integer pageSize, SortOrder sortOrder) throws UseException
    {
        Stream<Account> stream = accountsRepository.all().filter(stream2 -> stream2.getName().contains(searchValue));
        if (sortOrder.equals(SortOrder.AccountName))
        {
            stream = stream.sorted(Comparator.comparing(Account::getName));
        }
        if (userId != null)
        {
            stream = stream.filter(res -> res.getOwner().getId().equals(userId)
                    || res.getUsers().anyMatch(user1 -> user1.getId().equals(userId)));
        }
        return ListUtils.applyPage(stream, pageNumber, pageSize);
    }
}

