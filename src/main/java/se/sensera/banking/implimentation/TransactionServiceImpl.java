package se.sensera.banking.implimentation;

import lombok.AllArgsConstructor;
import se.sensera.banking.*;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;


public class TransactionServiceImpl implements TransactionService
{
    static final Object sync = new Object();
    UsersRepository usersRepository;
    AccountsRepository accountsRepository;
    TransactionsRepository transactionsRepository;
    static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    List<Consumer<Transaction>> consumerTransactionStream = new ArrayList<>();

    public TransactionServiceImpl(UsersRepository usersRepository, AccountsRepository accountsRepository, TransactionsRepository transactionsRepository) {
        this.usersRepository = usersRepository;
        this.accountsRepository = accountsRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @Override
    public Transaction createTransaction(String created, String userId, String accountId, double amount) throws UseException {
        Date date = parseDate(created);
        //CheckTransactions(account,user,userId,created,accountId,amount);
        TransactionImpl transaction = new TransactionImpl(UUID.randomUUID().toString(), date, checkUserStatus(userId), checkAccountStatus(userId, accountId),amount);
        if (transaction.getAmount() < 0)
        {
            if (transaction.getAmount() + sum(created, userId, accountId) < 0)
            {
                throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FUNDED);
            }
        }
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                approveConsumerTransactionStream(transaction);
            }
        }).start();
        return transactionsRepository.save(transaction);
    }

    public User checkUserStatus (String userId) throws UseException
    {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.USER_NOT_FOUND));
        if (!user.isActive())
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ACTIVE);
        return user;
    }

    private Account checkAccountStatus (String userId, String accountId) throws UseException
    {
        Account account = accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FOUND));
        if (!account.getOwner().getId().equals(userId) && account.getUsers().noneMatch(user -> user.getId().equals(userId)))
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ALLOWED);
        if (!account.isActive())
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ACTIVE);
        return account;
    }

    public Date parseDate(String stringDate) throws UseException
    {
        try {
            synchronized (sync)
            {
                return formatter.parse(stringDate);
            }
        } catch (ParseException e)
        {
            throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_FOUND);
        }
    }

    @Override
    public double sum(String created, String userId, String accountId) throws UseException
    {
        Account account = getAccount(accountId);
        verifyUser(account, userId);
        Date date = parseDate(created);
        return transactionsRepository.all()
                .filter(transaction -> transaction.getAccount().getId().equals(accountId))
                .filter(transaction -> transaction.getCreated().compareTo(date) <= 0)
                .mapToDouble(Transaction::getAmount).sum();
    }

    public Account getAccount(String accountId) throws UseException
    {
        return accountsRepository.getEntityById(accountId)
                .orElseThrow(() -> new UseException(Activity.CREATE_ACCOUNT, UseExceptionType.NOT_FOUND));
    }

    public void verifyUser(Account account, String userId) throws UseException
    {
        if (!account.getOwner().getId().equals(userId) && account.getUsers().noneMatch
                (user -> user.getId().equals(userId))) {
            throw new UseException(Activity.SUM_TRANSACTION, UseExceptionType.NOT_ALLOWED);
        }
    }

    public void approveConsumerTransactionStream(Transaction transaction)
    {
        try {
            consumerTransactionStream.forEach(transactionConsumer -> transactionConsumer.accept(transaction));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addMonitor(Consumer<Transaction> monitor)
    {
        consumerTransactionStream.add(monitor);
    }

    //
//    public Transaction CheckTransactions(Account account, User user, String userId, String created, String accountId, double amount) throws UseException {
//        if (!account.getOwner().equals(user))
//        {
//            if (account.getUsers().noneMatch(res -> res.getId().equals(userId)))
//            {
//                throw new UseException(Activity.CREATE_TRANSACTION, UseExceptionType.NOT_ALLOWED);
//            }
//        }
//        return null;
//    }
}