package se.sensera.banking.implimentation;

import se.sensera.banking.User;
import se.sensera.banking.UserService;
import se.sensera.banking.UsersRepository;
import se.sensera.banking.exceptions.Activity;
import se.sensera.banking.exceptions.UseException;
import se.sensera.banking.exceptions.UseExceptionType;
import se.sensera.banking.utils.ListUtils;

import java.util.Comparator;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.stream.Stream;


public class UserServiceImpl implements UserService
{
    UsersRepository usersRepository;

    public UserServiceImpl (UsersRepository usersRepository)
    {
        this.usersRepository = usersRepository;
    }

    @Override
    public User createUser(String name, String personalIdentificationNumber) throws UseException
    {
        if (usersRepository.all().anyMatch(user -> user.getPersonalIdentificationNumber().equalsIgnoreCase(personalIdentificationNumber)))
            throw new UseException(Activity.CREATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
        UserImpl user = new UserImpl(UUID.randomUUID().toString(), name, personalIdentificationNumber, true);
        usersRepository.save(user);
        return user;
    }

    @Override
    public User changeUser(String userId, Consumer<ChangeUser> changeUser) throws UseException
    {
        User user = usersRepository.getEntityById(userId).orElseThrow(() -> new UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        AtomicBoolean saveUser = new AtomicBoolean(false);
        changeUser.accept(new ChangeUser()
        {
            @Override
            public void setName(String name)
            {
                user.setName(name);
                saveUser.set(true);
            }

            @Override
            public void setPersonalIdentificationNumber(String personalIdentificationNumber) throws UseException
            {
                if (usersRepository.all().anyMatch(user -> user.getPersonalIdentificationNumber().equals(personalIdentificationNumber)))
                    throw new UseException(Activity.UPDATE_USER, UseExceptionType.USER_PERSONAL_ID_NOT_UNIQUE);
                user.setPersonalIdentificationNumber(personalIdentificationNumber);
                saveUser.set(true);
            }
        });
        if(saveUser.get())
        {
            return usersRepository.save(user);
        }
        return user;
    }


    @Override
    public User inactivateUser(String userId) throws UseException
    {
        User user = usersRepository.getEntityById(userId)
                .orElseThrow(() -> new  UseException(Activity.UPDATE_USER, UseExceptionType.NOT_FOUND));
        user.setActive(false);
        return usersRepository.save(user);
    }

    @Override
    public Optional getUser(String userId)
    {
        Optional user = usersRepository.getEntityById(userId);
        return user;
    }

    @Override
    public Stream<User> find(String searchString, Integer pageNumber, Integer pageSize, SortOrder sortOrder)
    {
        Stream<User> result = usersRepository.all().filter(user -> user.getName().toLowerCase().contains(searchString)).filter(User::isActive);
        if (sortOrder == UserService.SortOrder.Name)
            result = result.sorted(Comparator.comparing(User::getName));
        return findSieve (result,pageNumber,pageSize,sortOrder);
    }

    public Stream<User> findSieve (Stream<User> result, Integer pageNumber, Integer pageSize, SortOrder sortOrder)
    {
        if (sortOrder == UserService.SortOrder.PersonalId)
            result = result.sorted(Comparator.comparing(User::getPersonalIdentificationNumber));
        return ListUtils.applyPage(result, pageNumber, pageSize);
    }

}
// COMMIT BEFORE MOVING ON!!

